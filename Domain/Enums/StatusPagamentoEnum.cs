﻿namespace Payment.Domain.Enums
{
    public enum StatusPagamentoEnum
    {
        Cancelada,
        AguardandoPagamento,
        PagamentoAprovado,
        EnviadoTransportadora,
        Entregue,

    }
}
