﻿using System.ComponentModel.DataAnnotations;

namespace Payment.Domain.Models
{
    public class Item
    {
        [Key]
        public int Id { get; set; }
        public string? Nome { get; set; }
        public int Preco { get; set; }

    }
}
