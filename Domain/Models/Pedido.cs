﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Payment.Domain.Models
{
    public class Pedido
    {
        [Key]
        public int Id { get; set; }

        public List<Item> ItensVendidos { get; set; }
    }
}
