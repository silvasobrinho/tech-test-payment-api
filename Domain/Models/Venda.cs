﻿using Payment.Domain.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Payment.Domain.Models
{
    public class Venda
    {
        public Venda()
        {
        }
        public Venda(DateTime datavenda, Vendedor vendedor, Pedido pedido)
        {
            DataVenda = datavenda;
            Vendedor = vendedor;
            Pedido = pedido;
        }
        [Key]
        public int Id { get; set; }
        public int VendedorId { get; set; }
        [ForeignKey("VendedorId")]
        public Vendedor Vendedor { get; set; }
        public int PedidoId { get; set; }
        [ForeignKey("PedidoId")]
        public Pedido Pedido { get; set; }
        public DateTime DataVenda { get; set; }
        public StatusPagamentoEnum status { get; set; }

    }
}
