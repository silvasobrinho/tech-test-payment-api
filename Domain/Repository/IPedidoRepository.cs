﻿using Payment.Domain.Models;
using System.Collections.Generic;

namespace Payment.Domain.Repository
{
    public interface IPedidoRepository
    {
        public List<Pedido> GetPedidos();
        public Pedido GetPedidosById(int id);
        public void SavePedidos(Pedido pedido);
        public void UpdatePedidos(Pedido pedido);
        public void DeletePedidos(int id);
    }
}
