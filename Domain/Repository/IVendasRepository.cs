﻿using Payment.Domain.Models;
using System.Collections.Generic;

namespace Payment.Domain.Repository
{
    public interface IVendasRepository
    {
        public List<Venda> GetVendas();
        public Venda GetVendasById(int id);
        public int SaveVendas(Venda venda);
        public void UpdateVendas(Venda venda);
        public void DeleteVendas(int id);

    }
}
