﻿using Payment.Domain.Models;
using System.Collections.Generic;

namespace Payment.Domain.Repository
{
    public interface IVendedoresRepository
    {
        public List<Vendedor> GetVendedores();
        public Vendedor GetVendedoresById(int id);
        public void SaveVendedores(Vendedor vendedor);
        public void UpdateVendedores(Vendedor vendedor);
        public void DeleteVendedores(int id);
    }
}
