﻿using AutoMapper;
using Payment.Application.ViewModel;
using Payment.Domain.Models;

namespace Payment.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Venda, VendaGravarViewModel>();
        }
    }
}
