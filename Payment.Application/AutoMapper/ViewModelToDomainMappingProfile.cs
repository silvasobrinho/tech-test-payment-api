﻿using AutoMapper;
using Payment.Application.ViewModel;
using Payment.Domain.Models;

namespace Payment.Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<VendaGravarViewModel, Venda>()
                .ConstructUsing(c => new Venda(c.DataVenda, c.Vendedor, c.Pedido));

        }
    }
}