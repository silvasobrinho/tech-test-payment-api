﻿using Payment.Application.ViewModel;
using Payment.Domain.Enums;
using Payment.Domain.Models;
using System.Threading.Tasks;

namespace Payment.Application.Interfaces
{
    public interface IVendasAppService
    {
        Task<Venda> GetVendaById(int id);
        Task<int> RegistrarVenda(VendaGravarViewModel viewModel);
        Venda AtualizarVenda(StatusPagamentoEnum status, int idVenda);

    }
}
