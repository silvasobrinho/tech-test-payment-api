﻿using AutoMapper;
using Payment.Application.Interfaces;
using Payment.Application.ViewModel;
using Payment.Domain.Enums;
using Payment.Domain.Models;
using Payment.Domain.Repository;
using System.Threading.Tasks;

namespace Payment.Application.Services
{
    public class VendasAppService : IVendasAppService
    {
        private readonly IMapper _mapper;
        private readonly IVendasRepository _vendaRepo;

        public VendasAppService(IMapper mapper,
            IVendasRepository vendaRepo)
        {
            _mapper = mapper;
            _vendaRepo = vendaRepo;
        }

        public Venda AtualizarVenda(StatusPagamentoEnum status, int idVenda)
        {
            var vendaRepo = _vendaRepo.GetVendasById(idVenda);

            if (vendaRepo == null)
                return new Venda();

            vendaRepo.status = AtualizarStatus(vendaRepo.status, status);
            _vendaRepo.UpdateVendas(vendaRepo);
            return vendaRepo;
        }

        public async Task<Venda> GetVendaById(int id)
        {
            return _vendaRepo.GetVendasById(id);
        }

        public async Task<int> RegistrarVenda(VendaGravarViewModel viewModel)
        {
            var VendaMapeada = _mapper.Map<Venda>(viewModel);
            VendaMapeada.status = StatusPagamentoEnum.AguardandoPagamento;
            return _vendaRepo.SaveVendas(VendaMapeada);

        }

        private StatusPagamentoEnum AtualizarStatus(StatusPagamentoEnum statusAtual, StatusPagamentoEnum StatusAtualizado)
        {
            if (statusAtual == StatusPagamentoEnum.AguardandoPagamento)
                if (StatusAtualizado == StatusPagamentoEnum.PagamentoAprovado || StatusAtualizado == StatusPagamentoEnum.Cancelada)
                    return StatusAtualizado;

            if (statusAtual == StatusPagamentoEnum.PagamentoAprovado)
                if (StatusAtualizado == StatusPagamentoEnum.EnviadoTransportadora || StatusAtualizado == StatusPagamentoEnum.Cancelada)
                    return StatusAtualizado;

            if (statusAtual == StatusPagamentoEnum.EnviadoTransportadora && StatusAtualizado == StatusPagamentoEnum.Entregue)
                return StatusAtualizado;

            return statusAtual;
        }

    }
}
