﻿using Payment.Domain.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace Payment.Application.ViewModel
{
    public class VendaGravarViewModel
    {
        [Required(ErrorMessage = "Campo Vendedor é obrigatório")]
        public Vendedor? Vendedor { get; set; }
        [Required(ErrorMessage = "Campo de Pedido é obrigatório")]
        public Pedido? Pedido { get; set; }
        [Required(ErrorMessage = "Campo de Data da venda é obrigatório")]
        public DateTime DataVenda { get; set; }
    }
}
