﻿using Microsoft.EntityFrameworkCore;
using Payment.Domain.Models;

namespace Payment.Data.Context
{
    public class ApiContext : DbContext
    {
        protected override void OnConfiguring
       (DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase(databaseName: "PaymentApi");

        }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Pedido> Pedidos { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Venda>(x =>
            {
                x.HasOne(p => p.Vendedor).WithMany();
                x.HasOne(p => p.Pedido).WithOne();
            });

            modelBuilder.Entity<Vendedor>().HasKey(x => x.Id);

            modelBuilder.Entity<Item>().HasKey(x => x.Id);

            modelBuilder.Entity<Pedido>(x =>
            {
                x.HasKey(x => x.Id);
                x.HasMany(p => p.ItensVendidos).WithOne();

            });
        }
    }
}