﻿using Payment.Data.Context;
using Payment.Domain.Models;
using Payment.Domain.Repository;
using System.Collections.Generic;
using System.Linq;

namespace Payment.Data.Repository
{
    public class PedidoRepository : IPedidoRepository
    {
        public readonly ApiContext context;

        public PedidoRepository()
        {
            context = new ApiContext();
        }
        public void DeletePedidos(int id)
        {
            var pedido = context.Pedidos.Find(id);
            if (pedido != null)
            {
                context.Pedidos.Remove(pedido);
                context.SaveChanges();
            }

        }

        public List<Pedido> GetPedidos()
        {
            return context.Pedidos.ToList();
        }

        public Pedido GetPedidosById(int id)
        {
            return context.Pedidos.Find(id) ?? new Pedido();
        }

        public void SavePedidos(Pedido pedido)
        {
            context.Pedidos.Add(pedido);
            context.SaveChanges();
        }

        public void UpdatePedidos(Pedido pedido)
        {
            var pedidoNobanco = context.Pedidos.Find(pedido.Id);
            if (pedidoNobanco != null)
            {
                context.Pedidos.Update(pedido);
                context.SaveChanges();
            }
        }
    }
}
