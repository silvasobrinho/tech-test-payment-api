﻿using Payment.Data.Context;
using Payment.Domain.Models;
using Payment.Domain.Repository;
using System.Collections.Generic;
using System.Linq;

namespace Payment.Data.Repository
{
    public class VendasRepository : IVendasRepository
    {
        public readonly ApiContext context;

        public VendasRepository()
        {
            context = new ApiContext();
        }
        public void DeleteVendas(int id)
        {
            var venda = context.Vendas.Find(id);
            if (venda != null)
            {
                context.Vendas.Remove(venda);
                context.SaveChanges();
            }
        }

        public List<Venda> GetVendas()
        {
            return context.Vendas.ToList();
        }

        public Venda GetVendasById(int id)
        {
            return context.Vendas.Find(id) ?? new Venda();
        }

        public int SaveVendas(Venda venda)
        {
            context.Vendas.Add(venda);
            return context.SaveChanges();
        }

        public void UpdateVendas(Venda venda)
        {
            var pedidoNobanco = context.Vendas.Find(venda.Id);
            if (pedidoNobanco != null)
            {
                context.Vendas.Update(venda);
                context.SaveChanges();
            }
        }
    }
}
