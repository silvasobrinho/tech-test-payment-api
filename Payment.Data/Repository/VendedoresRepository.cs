﻿using Payment.Data.Context;
using Payment.Domain.Models;
using Payment.Domain.Repository;
using System.Collections.Generic;
using System.Linq;

namespace Payment.Data.Repository
{
    public class VendedoresRepository : IVendedoresRepository
    {
        public readonly ApiContext context;

        public VendedoresRepository()
        {
            context = new ApiContext();
        }

        public void DeleteVendedores(int id)
        {
            var vendedorbanco = context.Vendedores.Find(id);
            if (vendedorbanco != null)
            {
                context.Vendedores.Remove(vendedorbanco);
                context.SaveChanges();
            }
        }

        public List<Vendedor> GetVendedores()
        {
            return context.Vendedores.ToList();
        }

        public Vendedor GetVendedoresById(int id)
        {
            return context.Vendedores.Find(id) ?? new Vendedor();
        }

        public void SaveVendedores(Vendedor vendedor)
        {
            context.Vendedores.Add(vendedor);
            context.SaveChanges();
        }

        public void UpdateVendedores(Vendedor vendedor)
        {
            var vendedorbanco = context.Vendedores.Find(vendedor.Id);
            if (vendedorbanco != null)
            {
                context.Vendedores.Update(vendedor);
                context.SaveChanges();
            }
        }
    }
}
