﻿using Microsoft.Extensions.DependencyInjection;
using Payment.Application.Interfaces;
using Payment.Application.Services;
using Payment.Data.Repository;
using Payment.Domain.Repository;

namespace Payment.DependencyInjection
{
    public static class DependencyInjectionRegister
    {
        public static void RegisterServices(IServiceCollection services)
        {

            services.AddScoped<IPedidoRepository, PedidoRepository>();
            services.AddScoped<IVendasRepository, VendasRepository>();
            services.AddScoped<IVendedoresRepository, VendedoresRepository>();

            services.AddScoped<IVendasAppService, VendasAppService>();

        }
    }
}
