﻿using Payment.DependencyInjection;

namespace Payment.Api.Configurations
{
    public static class DependencyInjectionConfig
    {
        public static void AddDependencyInjectionConfiguration(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            DependencyInjectionRegister.RegisterServices(services);
        }
    }
}
