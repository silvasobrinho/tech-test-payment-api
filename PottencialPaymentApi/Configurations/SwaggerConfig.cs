﻿using Microsoft.OpenApi.Models;

namespace Payment.Api.Configurations
{
    public static class SwaggerConfig
    {
        public static void AddSwaggerConfiguration(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddSwaggerGen(s =>
            {
                s.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "PottencialPaymentApi Project",
                    Description = "Documentação da api PottencialPaymentApi",
                    Contact = new OpenApiContact { Name = "Giovane Sobrinho", Email = "giovane.sobrinho@gmail.com", Url = new Uri("https://www.linkedin.com/in/giovane-da-silva-sobrinho-7b9427168/") },
                    License = new OpenApiLicense { Name = "MIT", Url = new Uri("https://github.com/git/git-scm.com/blob/main/MIT-LICENSE.txt") }
                });
            });
        }

        public static void UseSwaggerSetup(this IApplicationBuilder app)
        {
            if (app == null) throw new ArgumentNullException(nameof(app));

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
            });
        }
    }
}