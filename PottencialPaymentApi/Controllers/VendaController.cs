﻿using Microsoft.AspNetCore.Mvc;
using Payment.Application.Interfaces;
using Payment.Application.ViewModel;
using Payment.Domain.Enums;

namespace Payment.Api.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        public readonly IVendasAppService _service;
        public VendaController(IVendasAppService service)
        {
            _service = service;
        }

        [HttpPost]
        public IActionResult RegistrarVenda(VendaGravarViewModel view)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            if (!view.Pedido.ItensVendidos.Any())
                return BadRequest(" A venda Deve conter pelo menos um item vendido!");
            _service.RegistrarVenda(view);

            return Ok();
        }

        [HttpGet]
        public IActionResult BuscarVenda(int id)
        {
            var venda = _service.GetVendaById(id);

            if (venda.Id == 0)
                return NotFound();
            return Ok(venda);
        }

        [HttpPut]
        public IActionResult AtualizarVenda(StatusPagamentoEnum status, int idVenda)
        {
            var atualizacao = _service.AtualizarVenda(status, idVenda);
            return Ok(atualizacao);
        }
    }
}
