﻿using AutoMapper;
using Moq;
using Payment.Application.Services;
using Payment.Application.ViewModel;
using Payment.Domain.Enums;
using Payment.Domain.Models;
using Payment.Domain.Repository;
using Xunit;

namespace PottencialPaymentUnitTests.Services
{
    public class VendasAppServiceTests
    {
        private readonly Mock<IMapper> _mapper;
        private readonly Mock<IVendasRepository> _vendaRepo;
        private readonly VendasAppService _vendaService;

        public VendasAppServiceTests()
        {
            _vendaRepo = new Mock<IVendasRepository>();
            _mapper = new Mock<IMapper>();

            _vendaRepo.Setup(x => x.SaveVendas(It.IsAny<Venda>())).Returns(1);
            _vendaRepo.Setup(x => x.UpdateVendas(It.IsAny<Venda>()));
            _vendaRepo.Setup(x => x.GetVendasById(It.IsAny<int>())).Returns(new Venda() { DataVenda = DateTime.Now.AddDays(-10), status = StatusPagamentoEnum.PagamentoAprovado });

            _vendaService = new VendasAppService(_mapper.Object, _vendaRepo.Object);
        }


        [Fact]
        public void DeveRegistrar()
        {
            var view = new VendaGravarViewModel() { DataVenda = DateTime.Now };
            var serviceResponse = _vendaService.RegistrarVenda(view).IsCompleted;

            Assert.True(serviceResponse);
        }

        [Fact]
        public void DeveAtualizarStatusComsucesso()
        {

            var serviceResponse = _vendaService.AtualizarVenda(StatusPagamentoEnum.AguardandoPagamento, 1);

            Assert.NotNull(serviceResponse);
        }

        [Fact]
        public void DeveBuscarPorId()
        {

            var serviceResponse = _vendaService.GetVendaById(1);

            Assert.NotNull(serviceResponse);
        }
    }
}
